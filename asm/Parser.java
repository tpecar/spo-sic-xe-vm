/* Tadej Pečar, SPO 2016/2017 */
package asm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;

public class Parser {
    
    
    // dokler ne zgradimo pravega vmesnika
    public static void main(String[] args) throws IOException {
        System.out.format("CWD %s\n", Paths.get("").toAbsolutePath().toString());
        BufferedReader in = new BufferedReader(new FileReader("./src/tests/addr-simple.asm"));
        
        while(in.ready())
            System.out.println(in.readLine());
    }
}
