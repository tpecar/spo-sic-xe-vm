/* Tadej Pečar, SPO 2016/2017 */
package asm;

/**
 * Predstavlja posamezno direktivo.
 * @author tpecar
 */
public interface Directive {
    /**
     * @return mnemonik za direktivo
     */
    String getMnemonic();
}
