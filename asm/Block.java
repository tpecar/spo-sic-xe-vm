/* Tadej Pečar, SPO 2016/2017 */
package asm;

import java.util.LinkedList;

/**
 * Definira posamezen kodni odsek, ki ga eksplicitno definiramo preko include
 * direktive.
 * 
 * Te hranimo znotraj HashMap, pri cemer je privzeti kodni odsek je dostopen
 * preko null kljuca.
 * 
 * @author tpecar
 */
public class Block {
    
    // ime kodnega odseka
    private final String name;
    // seznam vseh binarnih objektov, ki jih ta vsebuje
    // (tipicno je to koda, lahko pa so arbitrarne vrednosti, ki so jih dolocile
    // direktive)
    LinkedList<Blob> blobs = new LinkedList<>();
    
    public Block(String name) {
        this.name = name;
    }
    
    /**
     * @return ime kodnega odseka
     */
    String getName() {
        return name;
    }
}
