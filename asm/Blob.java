/* Tadej Pečar, SPO 2016/2017 */
package asm;

/**
 * Osnovni razred, ki predstavlja izhodni binarni objekt.
 * Ima znano lokacijo v pomnilniskem prostoru ter svojo dolzino.
 * @author tpecar
 */
public class Blob {
    
    private final int startAddress;
    private final int length;
    
    public Blob(int startAddress, int length) {
        this.startAddress = startAddress;
        this.length = length;
    }
}
