/* Tadej Pečar, SPO 2016/2017 */
package asm.symbol;

/**
 * Za vracanje napake v primeru nepodprte operacije med tipoma.
 * Tipicno vrnemo ko za dan Value tip v operacijo dobimo drugi Value tip.
 * @author tpecar
 */
public class ValueOperationException extends RuntimeException {
    public ValueOperationException(Value val1, Value val2) {
        super(String.format("Could not apply operation between %s and %s",
                val1.describeType(),
                val2.describeType()));
    }
    public ValueOperationException(String msg) {
        super("Operation invalid: "+msg);
    }
    public ValueOperationException() {
        super("Unsupported operation");
    }
}
