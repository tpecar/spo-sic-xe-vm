/* Tadej Pečar, SPO 2016/2017 */
package asm.symbol;

import java.util.LinkedList;

/**
 * Definira tabelo posameznega podatkovnega tipa.
 * Gre za genericen podatkovni tip.
 * @author tpecar
 */
public class TableValue extends Value {
    
    private final ValueType elementType;
    private LinkedList<Value> table;
    
    public TableValue(ValueType elemeType) {
        this.elementType = elemeType;
        this.table = new LinkedList<>();
    }
    
    @Override public ValueType getType() {return ValueType.TABLE;}
    @Override public String describeType() {return "<table of "+elementType.name()+">";}
    
    // da preverimo ce gre za ista tipa
    protected void check(Value val) {
        if(this.getType()!=val.getType() || this.elementType != ((TableValue)val).elementType)
            throw new ValueOperationException(this, val);
    }
    
    @Override public Value sum(Value val) {
        // zdruzi tabeli
        TableValue tbl = new TableValue(elementType);
        tbl.table.addAll(this.table);
        tbl.table.addAll(((TableValue)val).table);
        return tbl;
    }
    @Override public Value sub(Value val) {
        throw new ValueOperationException();
    }
    @Override public Value mul(Value val) {
        throw new ValueOperationException();
    }
    @Override public Value div(Value val) {
        throw new ValueOperationException();
    }
}
