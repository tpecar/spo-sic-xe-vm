/* Tadej Pečar, SPO 2016/2017 */
package asm.symbol.ast;

import asm.symbol.Value;

/**
 * Vozlisce abstraktnega sintaksnega drevesa.
 * @author tpecar
 */
public interface ASTNode {
    /**
     * Razresi vozlisce (si interno shrani svojo vrednost, ker lahko da ga bomo
     * poskusali razresiti veckrat) ter vrni primitivno vrednost.
     * @return 
     */
    Value getValue();
}
