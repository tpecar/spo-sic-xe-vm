/* Tadej Pečar, SPO 2016/2017 */
package asm.symbol.ast;

/**
 * Binarni operator, ki ima levega ter desnega sina.
 * Nad sinovoma izvedemo operacijo operatorja.
 * 
 * Preveri, ce je prioriteta potrebna tudi pri vozliscu s primitivnim
 * podatkovnim tipom.
 * 
 * Znaka ( ) se tretirata posebej - ta vplivata na dolocitev trenutno najvisje
 * prioritete pri stopanju v globino.
 * 
 * @author tpecar
 */
public abstract class ASTOperator implements ASTNode {
    public ASTNode left;
    public ASTNode right;

    // poleg vrednosti moramo vracati se prioriteto operatorja za primerjavo
    public abstract int getPriority();
}
