/* Tadej Pečar, SPO 2016/2017 */
package asm.symbol.ast;

import asm.symbol.Value;

/**
 * Vozlisce za sestevanje.
 * @author tpecar
 */
public class ASTSum extends ASTOperator {
    @Override public int getPriority() {return 1;}
    
    @Override public Value getValue() {
        return left.getValue().sum(right.getValue());
    }
}
