/* Tadej Pečar, SPO 2016/2017 */
package asm.symbol.ast;

import asm.symbol.Value;

/**
 * Vozlisce za odstevanje.
 * @author tpecar
 */
public class ASTSub extends ASTOperator {
    @Override public int getPriority() {return 1;}
    
    @Override public Value getValue() {
        return left.getValue().sub(right.getValue());
    }
}
