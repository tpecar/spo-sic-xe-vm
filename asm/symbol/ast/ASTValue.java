/* Tadej Pečar, SPO 2016/2017 */
package asm.symbol.ast;

import asm.symbol.Value;

/**
 * Vozlisce, ki vsebuje primitivno vrednost.
 * @author tpecar
 */
public class ASTValue implements ASTNode {
    private final Value value;
    
    public ASTValue(Value value) {
        this.value = value;
    }
    
    @Override public Value getValue() {return value;}
}
