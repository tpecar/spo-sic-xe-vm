/* Tadej Pečar, SPO 2016/2017 */
package asm.symbol.ast;

import asm.symbol.Value;

/**
 * Vozlisce za mnozenje.
 * @author tpecar
 */
public class ASTMul extends ASTOperator {
    @Override public int getPriority() {return 2;}
    
    @Override public Value getValue() {
        return left.getValue().mul(right.getValue());
    }
}
