/* Tadej Pečar, SPO 2016/2017 */
package asm.symbol;

/**
 * Nepredznacen celostevilski podatkovni tip.
 * 
 * Pretvorb zaenkrat ne omogocimo, ker bi morali bolj splosno tretirati tudi
 * relativnost vrednosti - prav tako bi morali ekvivalentne pretvorbe narediti
 * pri vseh podatkovnih tipih, med katerimi se lahko pretvarja.
 * 
 * @author tpecar
 */
public class FloatValue extends Value {
    public final double value;
    public FloatValue(double val) {
        this.value = val;
    }
    
    @Override public ValueType getType() {return ValueType.FLOAT;}
    
    @Override public String describeType() {return "<float>";}
    
    // operacije
    @Override public Value sum(Value val) {
        check(val);
        return new FloatValue(this.value + ((FloatValue)val).value);
    }
    @Override public Value sub(Value val) {
        check(val);
        return new FloatValue(this.value - ((FloatValue)val).value);
    }
    @Override public Value mul(Value val) {
        check(val);
        return new FloatValue(this.value * ((FloatValue)val).value);
    }
    @Override public Value div(Value val) {
        check(val);
        return new FloatValue(this.value / ((FloatValue)val).value);
    }
}
