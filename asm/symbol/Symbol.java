/* Tadej Pečar, SPO 2016/2017 */
package asm.symbol;

import asm.symbol.ast.ASTNode;

/**
 * Simbol, kateremu je prirejena (preko AST dolocena) kompleksna vrednost.
 * @author tpecar
 */
public class Symbol {
    // ime simbola
    private final String name;
    // vrstica, v kateri je ta prvic dolocena (lahko tudi stolpec, vendar bi
    // morali tvoriti svoj lexer, namesto da se zanasamo na Scanner)
    private final int columnDefined;
    
    // koren abstraktnega sintaksnega drevesa, ki definira njeno vrednost
    private ASTNode ast;
    // vrednost, ko ze evalviramo sintaksno drevo - dokler ga ne, je null
    // (to je hkrati indikator, da simbola se nismo razresili)
    private Value value;
    
    public Symbol(String name, int columnDefined) {
        this.name = name;
        this.columnDefined = columnDefined;
    }
}
