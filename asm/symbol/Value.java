/* Tadej Pečar, SPO 2016/2017 */
package asm.symbol;

/**
 * Primitivna vrednost, ki je lahko bodisi
 * - (ne)predznacena celostevilska vrednost
 * - znak
 * - plavajoca vejica ali
 * - tabela le-teh.
 * 
 * Ti implementirajo vse mozne operacije - pri tem te same izvajajo preverjanje
 * ter izracun. Pri tem NE moremo kombinirati operacij vrednosti s simboli - 
 * hierarhija je namrec taka, da dokler sam simbol ni razresen, nima value, ter
 * posledicno ne moremo izvesti nobene operacije nad njim.
 * 
 * Ko se razresi, ima definiran value, pri cemer je ta primitiv, nad katerim pa
 * lahko izvajamo operacije.
 * 
 * @author tpecar
 */
public abstract class Value {
    /**
     * @return tip primitivne vrednosti
     */
    public abstract ValueType getType();
    /**
     * @return podroben opis tipa - za namen napak, predvsem zavoljo tabel 
     */
    public abstract String describeType();
    
    /*
    Operacije med primitivi.
    Vsaka od teh vrne nov Value objekt, ki predstavlja rezultat operacije.
    */
    public abstract Value sum(Value val);
    public abstract Value sub(Value val);
    public abstract Value mul(Value val);
    public abstract Value div(Value val);
    
    // da preverimo ce gre za ista tipa
    protected void check(Value val) {
        if(val.getType()!=this.getType())
            throw new ValueOperationException(this, val);
    }
    
    // vracanje vrednosti je implementirano v vsakem tipu posebej
}
