/* Tadej Pečar, SPO 2016/2017 */
package asm.symbol;

/**
 * Nepredznacen celostevilski podatkovni tip.
 * @author tpecar
 */
public class IntegerValue extends Value {
    public final int value;
    
    // pove ali je dana vrednost relativna, tj. da je nastala na podlagi 
    // naslova lokacijskega stevca in je posledicno odvisna od prevedbe programa
    // - takih vrednosti ne smemo uporabljati pri izrazih, ki bi prav tako lahko
    //   vplivale na vsebino lokacijskega stevca
    // ostali tipi (zaenkrat) ne morejo nastopati v taki vlogi, zato to
    // definiramo le tu
    private final boolean relative;
    
    public IntegerValue(int val, boolean relative) {
        this.relative = relative;
        this.value = val;
    }
    
    @Override public ValueType getType() {return ValueType.INTEGER;}
    
    @Override public String describeType() {return "<unsigned value>";}
    
    // operacije
    @Override public Value sum(Value val) {
        check(val);
        if(this.relative && ((IntegerValue)val).relative)
            throw new ValueOperationException("both operands relative");
        return new IntegerValue(this.value + ((IntegerValue)val).value,
            this.relative || ((IntegerValue)val).relative);
    }
    @Override public Value sub(Value val) {
        check(val);
        // ce sta oba relativna, potem izhod ni relativen
        // ce ni nobeden od njiju relativen, potem izhod ni relativen
        // ce je natanko eden od njiju relativen, potem je tudi izhod relativen
        return new IntegerValue(this.value - ((IntegerValue)val).value,
            (this.relative ^ ((IntegerValue)val).relative));
    }
    // mnozenje ter deljenje sta veljavna le na nerelativnih izrazih
    @Override public Value mul(Value val) {
        check(val);
        if(this.relative || ((IntegerValue)val).relative)
            throw new ValueOperationException("relative operand(s)");
        return new IntegerValue(this.value * ((IntegerValue)val).value, false);
    }
    @Override public Value div(Value val) {
        check(val);
        if(this.relative || ((IntegerValue)val).relative)
            throw new ValueOperationException("relative operand(s)");
        return new IntegerValue(this.value / ((IntegerValue)val).value, false);
    }
}
