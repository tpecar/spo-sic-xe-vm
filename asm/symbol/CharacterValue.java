/* Tadej Pečar, SPO 2016/2017 */
package asm.symbol;

/**
 * Nepredznacen celostevilski podatkovni tip.
 * @author tpecar
 */
public class CharacterValue extends Value {
    public final char value;
    public CharacterValue(char val) {
        this.value = val;
    }
    
    @Override public ValueType getType() {return ValueType.CHAR;}
    
    @Override public String describeType() {return "<character>";}
    
    // operacije
    @Override public Value sum(Value val) {
        check(val);
        return new CharacterValue((char)(this.value + ((CharacterValue)val).value));
    }
    @Override public Value sub(Value val) {
        check(val);
        return new CharacterValue((char)(this.value - ((CharacterValue)val).value));
    }
    @Override public Value mul(Value val) {
        throw new UnsupportedOperationException();
    }
    @Override public Value div(Value val) {
        throw new UnsupportedOperationException();
    }
}
