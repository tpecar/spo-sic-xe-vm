/* Tadej Pečar, SPO 2016/2017 */
package asm.symbol;

/**
 * Veljavni tipi primitivnih vrednosti.
 * Uporabimo tudi za preverjanje ce smo dobili pravilen simbol za posamezno
 * kljucno besedo.
 * Tu se izvajajo vse operacije v javinih podatkovnih tipih, pri serializaciji
 * se sele pretvorijo v obliko za SIC/XE arhitekturo.
 * @author tpecar
 */
public enum ValueType {
    INTEGER,
    CHAR,
    FLOAT,
    TABLE
}
