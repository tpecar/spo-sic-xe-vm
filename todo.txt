
Trenutno je misljeno, da se sam virtualni stroj razdeli na
machine:
- Control
  nadzira dejansko izvajanje ukazov, skrbi za fetch/execute (morda bi se to
  lahko premaknilo v loceno podenoto?)
    - EventDispatcher
      skrbi da se za vsak dispatchEvent klicejo registrirane listener metode
- State
  trenutno stanje virtualnega stroja (registri, pomnilnik, i/o)
    - Register
      ovijalni razred, zaradi ukazov formata 2, ki lahko delajo nad poljubnimi
      registri (se nanje sklicujejo z indeksi)
- Instruction
  dejanski ukazi

gui:
- MainWindow
  poskrbi da se vzpostavi virtualni stroj ter da se nanj vezejo vsi ostali
  pogledi ter se vezejo tudi vsi potrebni eventi