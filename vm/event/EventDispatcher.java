/* Tadej Pečar, SPO 2016/2017 */
package vm.event;

import java.util.ArrayList;
import vm.machine.register.Register;

/**
 * Skrbi za obvescanje vseh odsekov programa glede notranjih dogodkov VM.
 * 
 * Dogodke tvori sam simulator, med tem ko so poslusalci teh dogodkov predvsem
 * razlicni deli uporabniskega vmesnika, ki morajo posodobiti svoj izgled glede
 * na svoje interno stanje.
 * 
 * Zaenkrat se lahko prozita dva dogodka
 *      registerEvent - zgodil se je dostop do registra
 *      memoryEvent - zgodil se je dostop do pomnilnika
 * Pri cemer zaenkrat zaradi lazje implementacije ne locujemo poslusateljev
 * glede na specifiko posameznega dostopa (recimo kaj oz. na kaksen nacin je
 * bilo kaj dostopano).
 * 
 * @author tpecar
 */
public class EventDispatcher {
    private static final ArrayList<MemoryEventListener> MEM_EVTS;
    private static final ArrayList<RegisterEventListener> REG_EVTS;
    /** Ali se naj EventDispatcher "odziva" na dogodke.
     *  Mi namrec pri hitrem izvajanju ne zelimo se vseh dogodkov, ker to
     *  bistveno upocasni simulacijo.
     */
    private static boolean eventRegistration;
    
    // gre za globalen objekt, zato ga inicializiramo ze ob startu
    static {
        MEM_EVTS = new ArrayList<>();
        REG_EVTS = new ArrayList<>();
        
        eventRegistration = true;
    }
    /**
     * Nastavimo ali se odzivamo na dogodke.
     * @param eventRegistration 
     */
    synchronized public static void setEventRegistration(boolean eventRegistration) {
        EventDispatcher.eventRegistration = eventRegistration;
    }
    /**
     * Poklice vse poslusatelje memory event.
     * @param mem pomnilnik, ki hrani dostopane vrednosti
     * @param from naslov od (vkljucno) katerega se dostopa
     * @param to naslov do (NE vkljucno) katerega se dostopa
     * @param ma razlog dostopa
     * @param t nacin dostopa (branje/pisanje)
     */
    synchronized public static void dispatchMemoryEvent(byte[] mem, int from, int to,
                                           MemoryAccess ma, Access t) {
        if(eventRegistration)
            for(MemoryEventListener mel : MEM_EVTS)
                mel.actionPerformed(mem, from, to, ma, t);
    }
    /**
     * Poklice vse poslusatelje register event.
     * @param reg register do katerega se dostopa
     * @param t nacin dostopa (branje/pisanje)
     */
    synchronized public static void dispatchRegisterEvent(Register reg, Access t) {
        if(eventRegistration)
            for(RegisterEventListener rel : REG_EVTS)
                rel.actionPerformed(reg, t);
    }
    /**
     * Doda poslusalca dogodkov dostopa do pomnilnika v seznam.
     * @param mel 
     */
    public static void addMemoryEventListener(MemoryEventListener mel) {
        MEM_EVTS.add(mel);
    }
    /**
     * Doda poslusalca dogodkov dostopa do registrov v seznam.
     * @param rel 
     */
    public static void addRegisterEventListener(RegisterEventListener rel) {
        REG_EVTS.add(rel);
    }
}
