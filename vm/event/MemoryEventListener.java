/* Tadej Pečar, SPO 2016/2017 */
package vm.event;

/**
 * Poslusalec za memory event.
 * 
 * @author tpecar
 */
public interface MemoryEventListener {
    // Pri poimenovanju metode se nekako zgledujemo po Javinih lastnih event
    // listenerjih
    /**
     * Se odzove na memory event.
     * Klice ga EventDispatcher.
     * 
     * @param mem pomnilnik, ki vsebuje spremenjene vrednosti - tega je bolj
     *            smiselno podati zraven, ker ce bi posamezni odseki hranili
     *            stanje posebej, bi se lahko v primeru zamenjave stanja
     *            zgodilo, da odseki delajo na starem stanju
     * @param from od (vkljucno) katerega naslova se je dostopal pomnilnik
     * @param to do (NE vkljucno) katerega naslova se je dostopal pomnilnik
     * 
     * Gre torej za obmocje, podano kot [from, to) da lazje tvorimo pogoje pri
     * zankah ipd.
     * 
     * @param ma razlog pomnilniskega dostopa (ali se je dostopal ukaz,
     *           enostavni, posredni operand)
     * @param t za kaksen dostop je slo (branje, pisanje) 
     */
    public void actionPerformed(byte[] mem, int from, int to, MemoryAccess ma, Access t);
}
