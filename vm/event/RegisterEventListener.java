/* Tadej Pečar, SPO 2016/2017 */
package vm.event;

import vm.machine.register.Register;

/**
 * Poslusalec za RegisterEvent.
 * 
 * @author tpecar
 */
public interface RegisterEventListener {
    /**
     * Se odzove na register event.
     * Klice ga EventDispatcher.
     * 
     * @param reg dostopan register
     * @param t za kaksen dostop je slo (branje, pisanje)
     */
    public void actionPerformed(Register reg, Access t);
}
