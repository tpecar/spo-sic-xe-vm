/* Tadej Pečar, SPO 2016/2017 */
package vm.event;

/**
 * Za kaksen tip dostopa gre.
 * @author tpecar
 */
public enum Access {
    READ,
    WRITE
}
