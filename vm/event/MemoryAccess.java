/* Tadej Pečar, SPO 2016/2017 */
package vm.event;

/**
 *
 * @author tpecar
 */
public enum MemoryAccess {
    INSTRUCTION,
    SIMPLE_OPERAND,
    INDIRECT_OPERAND_PTR,
    INDIRECT_OPERAND_VALUE
}
