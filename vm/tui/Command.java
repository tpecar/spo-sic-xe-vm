/* Tadej Pečar, SPO 2016/2017 */
package vm.tui;

/**
 * Predstavlja ukaz, ki ga lahko izvedemo v ukazni vrstici
 * (Terminal User Interface - TUI).
 * @author tpecar
 */
public interface Command {
    /**
     * @return ime ukaza
     */
    String getName();
    /**
     * @return opis ukaza 
     */
    String getDescription();
    /**
     * @return parametri ukaza (za opis)
     */
    String[] getArguments();
    /**
     * Operacija, ki ga ukaz izvede.
     */
    void execute();
}
