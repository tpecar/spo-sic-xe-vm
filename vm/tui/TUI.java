/* Tadej Pečar, SPO 2016/2017 */
package vm.tui;

import vm.event.Access;
import vm.event.EventDispatcher;
import vm.event.MemoryAccess;
import vm.event.MemoryEventListener;
import vm.event.RegisterEventListener;
import java.io.IOException;
import java.util.Scanner;
import vm.machine.Instruction;
import vm.machine.InstructionSet;
import vm.machine.Machine;
import vm.machine.Operand2;
import vm.machine.Operand34Interpreter;
import vm.machine.register.Register;
import vm.machine.register.RegisterFloat;

/**
 * Nekaksen zelo enostaven priblizek vmesniku za virtualni stroj.
 * Za namen testiranja.
 * @author tpecar
 */
public class TUI {
    static Machine machine;
    
    static {
        // ustvarimo instanco vm
        machine = new Machine();
        
        // dodamo poslusalce
        EventDispatcher.addRegisterEventListener(new RegisterEventListener() {
            @Override
            public void actionPerformed(Register reg, Access t) {
                if(reg.getIndex()==machine.state.F.getIndex())
                    System.out.format("[REG] %5s %-4s val 0x%012X (%f)\n",
                            t.name(), reg.toString(),
                            ((RegisterFloat)reg).get48(),
                            ((RegisterFloat)reg).get48double());
                else
                    System.out.format("[REG] %5s %-4s val 0x%06X (%d)\n",
                            t.name(), reg.toString(), reg.get24(), reg.get24());
            }
        });
        EventDispatcher.addMemoryEventListener(new MemoryEventListener() {
            @Override
            public void actionPerformed(byte[] mem, int from, int to, MemoryAccess ma, Access t) {
                System.out.format("[MEM] %5s [%05X - %05X, len %d] %15s val ",
                        t.name(), from, to-1, to-from, ma.name());
                int numBytes=to-from;
                long value=0;
                for(int cByte=0; cByte<numBytes; cByte++) {
                    System.out.format("%02X ",mem[from+cByte]);
                    value=value<<8;
                    value+=((int)mem[from+cByte])&0xFF;
                }
                System.out.format("(%4d)",value);
                // ce gre za instrukcijo, jo dekodiramo
                if(ma == MemoryAccess.INSTRUCTION) {
                    System.out.print(" -> ");
                    Instruction inst = machine.state.getInstruction(from);
                    switch (inst.getType()) {
                        case TYPE1 : System.out.format("T1 %s\n",
                                inst.getMnemonic());
                                break;
                        case TYPE2 : {
                            Operand2 op = new Operand2(machine.state, from);
                            if(inst==InstructionSet.INSTRUCTION[0x29] ||
                               inst==InstructionSet.INSTRUCTION[0x2A])
                                System.out.format("T2 %s %s, %d\n",
                                    inst.getMnemonic(),
                                    op.describeRegister1(),
                                    op.r2idx+1
                                );
                            else
                                System.out.format("T2 %s %s, %s\n",
                                    inst.getMnemonic(),
                                    op.describeRegister1(),
                                    op.describeRegister2()
                                );
                        } break;
                        case TYPE34 : {
                            Operand34Interpreter oi =
                                    new Operand34Interpreter(machine.state.mem,
                                            from,
                                            machine.state.oldPC.get24(),
                                            machine.state.B.get24(),
                                            machine.state.X.get24()
                                    );
                            System.out.format("T3 %s %s(0x%05X=%d)%s TA: 0x%05X=%d\n",
                                inst.getMnemonic(),
                                oi.getDescriptionPrefix(),
                                oi.getDisplacement(),
                                oi.getDisplacement(),
                                oi.getDescriptionPostfix(),
                                oi.getTA(),
                                oi.getTA()
                                );
                        }
                    }
                }
                else
                    System.out.println();
            }
        });
    }
    /** za branje vhoda */
    private static Scanner in = new Scanner(System.in);
    /** ali izvajamo REPL */
    private static boolean running=true;
    /** ukazi TUI */
    private static final Command COMMANDS[] = {
        new Command(){
            @Override public String getName(){return "s";}
            @Override public String getDescription(){return "single step";}
            @Override public String[] getArguments(){return null;}
            @Override public void execute() {
                machine.executeStep();
            }
        },
        new Command(){
            String args[] = {"[number of steps to be executed]"};
            @Override public String getName(){return "sm";}
            @Override public String getDescription(){return "step multiple";}
            @Override public String[] getArguments(){return args;}
            @Override public void execute() {
                int numSteps = in.nextInt();
                for(int cStep=0; cStep<numSteps; cStep++)
                    machine.executeStep();
            }
        },
        new Command(){
            @Override public String getName(){return "run";}
            @Override public String getDescription(){return "run until halt";}
            @Override public String[] getArguments(){return null;}
            @Override public void execute() {
                int numExec = machine.run();
                System.out.format("%d instructions executed\n", numExec);
            }
        },
        new Command(){
            @Override public String getName(){return "rs";}
            @Override public String getDescription(){return "register status";}
            @Override public String[] getArguments(){return null;}
            @Override public void execute() {
                for(int cReg=0; cReg<7; cReg++)
                      System.out.format("%2s: %06X (%d)\n",
                              machine.state.register[cReg].toString(),
                              machine.state.register[cReg].get24(),
                              machine.state.register[cReg].get24());
                  System.out.format("SW: %06X (%d)\n",
                              machine.state.SW.get24(),
                              machine.state.SW.get24());
                  System.out.format(" F: %012X (%f)\n",
                          machine.state.F.get48(),
                          machine.state.F.get48double());
            }
        },
        new Command(){
            String args[] = {"[register name]", "[value]"};
            @Override public String getName(){return "rw";}
            @Override public String getDescription(){return "register write";}
            @Override public String[] getArguments(){return args;}
            @Override public void execute() {
                String targetReg = in.next().toUpperCase();
                // poiscemo register
                for(int cReg=0; cReg<7; cReg++)
                    if(machine.state.register[cReg].toString().equals(targetReg))
                        machine.state.register[cReg].set24(Integer.decode(in.next()));
                if(targetReg.equals(machine.state.SW.toString()))
                    machine.state.SW.set24(Integer.decode(in.next()));
                if(targetReg.equals(machine.state.F.toString()))
                    machine.state.F.set48double(in.nextDouble());
            }
        },
        new Command(){
            String args[] = {"[start address]", "[length]"};
            @Override public String getName(){return "mr";}
            @Override public String getDescription(){return "memory read";}
            @Override public String[] getArguments(){return args;}
            @Override public void execute() {
                int from = Integer.decode(in.next());
                int numBytes = Integer.decode(in.next());

                for(int cByte=0; cByte<numBytes; cByte++) {
                    System.out.format("%02X ",machine.state.mem[from+cByte]);
                }
                System.out.println();
            }
        },
        new Command(){
            String args[] = {"[dispatch: 0 - off, 1 - on]"};
            @Override public String getName(){return "ev";}
            @Override public String getDescription(){return "event dispatch";}
            @Override public String[] getArguments(){return args;}
            @Override public void execute() {
                EventDispatcher.setEventRegistration(in.nextInt()==1);
            }
        },
        new Command(){
            @Override public String getName(){return "q";}
            @Override public String getDescription(){return "quit";}
            @Override public String[] getArguments(){return null;}
            @Override public void execute() {
                running = false;
            }
        },
        new Command(){
            @Override public String getName(){return "h";}
            @Override public String getDescription(){return "command help";}
            @Override public String[] getArguments(){return null;}
            @Override public void execute() {
                for(Command cmd : COMMANDS) {
                    System.out.format("%-15s : ",cmd.getDescription());
                    System.out.print(cmd.getName());
                    String[] args = cmd.getArguments();
                    if(args!=null)
                        for(String arg : args)
                            System.out.format(" %s",arg);
                    System.out.println();
                }
            }
        },
    };

    public static void main(String[] args) throws IOException {
        // prvi argument je pot do datoteke
        machine.loadObj(args[0]);
        // uspesno nalozili program, gremo v REPL
        System.out.println("Type 'h' for command help.");
        
        while(running) {
            System.out.format("[PC: %06X]> ",machine.state.PC.get24());
            String cmdName = in.next();
            
            boolean commandFound = false;
            for(Command cmd : COMMANDS) {
                if(cmd.getName().equals(cmdName)) {
                    commandFound=true;
                    cmd.execute();
                    break;
                }
            }
            if(!commandFound)
                System.out.format("Unknown command [%s]\n", cmdName);
        }
    }
}
