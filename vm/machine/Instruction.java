/* Tadej Pečar, SPO 2016/2017 */
package vm.machine;

/**
 * Predstavlja posamezen ukaz.
 * To je krovni razred, preko katerega ugotovimo, katerea tipa je ukaz.
 * @author tpecar
 */
public interface Instruction {
    /**
     * Vrne tip ukaza.
     * @return tip ukaza
     */
    InstructionType getType();
    
    /**
     * Vrne mnemonik ukaza.
     * Za namen debuggerja.
     * @return mnemonik
     */
    String getMnemonic();
}
