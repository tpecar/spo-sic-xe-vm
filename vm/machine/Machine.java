/* Tadej Pečar, SPO 2016/2017 */
package vm.machine;

// dispatch tabela
import vm.event.Access;
import vm.event.EventDispatcher;
import vm.event.MemoryAccess;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Virtualni stroj.
 * 
 * @author tpecar
 */
public class Machine {
    public State state;
    
    public Machine() {
        state = new State();
    }
    
    /**
     * Interpretira ter zazene strojni ukaz, na katerega kaze PC.
     * 
     * Pri tem spremeni stanje programskega stevca (to naredi po "fetch" ciklu,
     * a pred "execute" ciklom, tj. pred izvajanjem dejanskega ukaza.)
     */
    public void executeStep() {
        // shranimo si trenutni pc
        state.oldPC.set24(state.PC.get24());
        // interpretiramo operacijsko kodo
        Instruction inst = state.getInstruction(state.PC.get24());
        if(inst!=null) {
            switch(inst.getType()) {
                case TYPE1:
                    // ukaz formata 1 je 1B
                    state.PC.set24(state.PC.get24()+1);
                    dispatchInstructionEvent();
                    ((Instruction1)inst).execute(state); break;
                case TYPE2: {
                    Operand2 op = new Operand2(state, state.PC.get24());
                    // ukaz formata 2 je 2B
                    state.PC.set24(state.PC.get24()+2);
                    dispatchInstructionEvent();
                    ((Instruction2)inst).execute(state,op);
                    break;
                }
                case TYPE34: {
                    Operand34 op = new Operand34(state, state.PC.get24());
                    // ukaz formata 3 je 3B, formata 4 pa 4B
                    state.PC.set24(state.PC.get24()+(op.interpreter.isExtended() ? 4 : 3));
                    dispatchInstructionEvent();
                    ((Instruction34)inst).execute(state,op);
                }
            }
        }
        else
            throw new UnsupportedOperationException(
                    String.format("[inst addr: %05X] Unsupported opcode: %02X",
                            state.PC.get24(),
                            (state.mem[state.PC.get24()]>>2) & 0x3F));
    }
    /* tvori dogodek dostopa do pomnilnika zaradi ukaza - ni ravno najlepse */
    private void dispatchInstructionEvent() {
        EventDispatcher.dispatchMemoryEvent(
                state.mem,
                state.oldPC.get24(),
                state.PC.get24(), MemoryAccess.INSTRUCTION, Access.READ);
    }
    /**
     * Izvaja, dokler ne pride do HCF zanke.
     * @return stevilo izvedenih ukazov
     */
    public int run() {
        int numExec=0;
        do {
            executeStep();
            numExec++;
        } while (state.oldPC.get24()!=state.PC.get24());
        return numExec;
    }
    /* te stvari bi bilo potrebno bolje strukturirati */
    /**
     * Nalozi objektno datoteko v pomnilnik.
     * Pri tem nastavi PC na podani absolutni naslov.
     * 
     * @param filePath pot do datoteke
     * @throws java.io.IOException
     */
    public void loadObj(String filePath) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(filePath));
        
        String line = in.readLine();
        if(line.charAt(0)!='H')
            throw new IOException(String.format("Invalid header! [expected H, got %c]",line.charAt(0)));
        // pridobimo nalagalni naslov
        int startAddr = Integer.parseInt(line.substring(7, 13), 16);
        // pridobimo ter interpretiramo T zapise
        line = in.readLine();
        while(line.charAt(0)=='T') {
            int TAddr = Integer.parseInt(line.substring(1, 7), 16);
            int numBytes = Integer.parseInt(line.substring(7, 9), 16);
            for(int cByte=0; cByte<numBytes; cByte++)
                state.mem[TAddr++] = (byte)Integer.parseInt(line.substring(9+cByte*2, 11+cByte*2), 16);
            line = in.readLine();
        }
        // prvi M zapis smo (morda) dobili ze v zanki za T zapise
        while(line.charAt(0)=='M') {
            // naslov, podatka, ki ga moramo prenasloviti
            int Maddr = Integer.parseInt(line.substring(1, 7), 16);
            // njegova dolzina (v stevilu nibblov, tj. 4 bitnih enot)
            int numNib = Integer.parseInt(line.substring(7, 9), 16);
            boolean oddNib = numNib%2!=0;
            
            // pri tem upostevas dejstvo, da bo nibble z najmanjso tezo
            // poravnan na byte (tj. naslednji nibble po naslovu navzgor bo del
            // drugega byte-a)
            // to je zato ker prenaslavljamo naslove v ukazih, ki pa so sami
            // poravnani na byte
            // posledicno lahko beremo/pisemo po byte-ih, nato pa pri najnizjem
            // byte-u odrezemo zgornjih 4 bitov ce imamo liho stevilo nibblov
            
            long value = 0; // vrednost, ki jo prenaslavljamo
            // naslov najvisjega byte-a, ki se se prenaslavlja
            int numBytes = numNib/2;
            
            // najnizji byte (MSB) obdelamo posebej
            value += oddNib ? state.mem[Maddr] & 0x0F : state.mem[Maddr];
            for(int cByte=1; cByte<=numBytes; cByte++) {
                value = value << 8;
                value += state.mem[Maddr + cByte];
            }
            // prenaslovimo
            value += startAddr;
            // zapisemo nazaj - najnizji byte obdelamo posebej (ga obdelamo
            // zadnjega)
            for(int cByte=numBytes; cByte>0; cByte--) {
                state.mem[Maddr+cByte] = (byte)value;
                value = value>>>8;
            }
            state.mem[Maddr] = (byte)(oddNib ?
                    ((state.mem[Maddr] & 0xF0) | (value & 0x0F)) : value & 0xFF);
            // preberemo naslednji M zapis, oz. E zapis
            line = in.readLine();
        }
        // zapis za konec (E) smo ze prebrali, le interpretiramo ga
        if(line.charAt(0)!='E')
            throw new IOException(String.format("Invalid trailer! [expected E, got %c]",line.charAt(0)));
        state.PC.set24(Integer.parseInt(line.substring(1, 7), 16));
    }
}
