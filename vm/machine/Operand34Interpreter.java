/* Tadej Pečar, SPO 2016/2017 */
package vm.machine;

/**
 *
 * @author tpecar
 */
public class Operand34Interpreter {
    
    /* ze loceni skupini bitov ni, xbpe */
    private final byte ni;
    private final byte xbpe;
    /* ze interpretiran odmik, uporabni naslov */
    private int displacement;
    private final boolean extended;
    private final boolean pcRelative;
    private final boolean baseRelative;
    private final boolean xRelative;
    private final int TA;
    private boolean TAValid; // lahko se namrec zgodi, da je z bp napacen
    
    public Operand34Interpreter(byte[] mem, int address, int PCval, int Bval,
                                int Xval) {
  
        // locimo bite
        ni = (byte)(mem[address] & 0x03);
        xbpe = (byte)((mem[address+1] & 0xF0)>>4);
        
        extended = (xbpe & 0x1)!=0;
        pcRelative = (xbpe & 0x2)!=0;
        baseRelative = (xbpe & 0x4)!=0;
        xRelative = (xbpe & 0x8)!=0;
        
        // pridobimo odmik glede na format ukaza
        // format S3 (15bit) - preverimo ni
        if (ni==0) {
            displacement = ((int)mem[address+1] & 0x7F)<<8 |
                            (int)mem[address+2];
            // samo v primeru pc-relativnega naslavljanja moramo obravnavati
            // naslov predznaceno
            if(pcRelative)
                displacement=displacement<<17>>17;
        }
        // format X3 (12bit) - preverimo e
        else if (!extended) {
            displacement = ((int)mem[address+1] & 0x0F)<<8 |
                            ((int)mem[address+2] & 0xFF);
            if(pcRelative)
                displacement=displacement<<20>>20;
        }
        // format 4 (20bit)
        else {
            displacement = ((int)mem[address+1] & 0x0F)<<16 |
                            ((int)mem[address+2] & 0xFF) <<8 |
                            ((int)mem[address+3] & 0xFF);
            if(pcRelative)
                displacement=displacement<<12>>12;
        }
        // Dolocitev uporabnega naslova - UN (TA).
        //
        // indeksiranje je mozno pri vseh nacinih naslavljanja, extended
        // teoreticno prav tako - zato se omejimo na bp bite
        
        // v primeru indeksnega naslavlanja pristejemo naslovu se vsebino
        // indeksnega registra (X)
        int xDisp = displacement + (xRelative ? Xval : 0);
        TAValid = true;
        // pristejemo se vsebino PC/B registrov - glede na bp
        switch((xbpe & 0x6)>>1) {
            // direct addressing - x00xb - odmik je ze UN
            case 0: TA = xDisp; break;
            // pc relative - x01xb
            case 1: TA = xDisp + PCval + (extended ? 4 : 3); break;
            // base relative - x10xb
            case 2: TA = xDisp + Bval; break;
            // preostali nacini (x11xb) so neveljavni
            default:
                TA = -1;
                TAValid = false;
        }
    }
    public int getDisplacement() {
        return displacement;
    }
    public boolean isValid() {
        return TAValid;
    }
    public int getTA() {
        return TA;
    }
    public byte getNI() {
        return ni;
    }
    public byte getXBPE() {
        return xbpe;
    }
    public boolean isExtended() {
        return extended;
    }
    public boolean isPCRelative() {
        return pcRelative;
    }
    public boolean isBaseRelative() {
        return baseRelative;
    }
    public boolean isXRelative() {
        return xRelative;
    }
    /* prefix, postfix, ki ga dodajamo mnemoniku - sam odmik tu ni vkljucen, saj
       ga lahko prikazemo na razlicne nacine */
    /**
     * Vrne prefix, ki ga je potrebno dodati mnemoniku ukaza.
     * Pri tem ne vrnemo izjeme v primeru napacnega nacina, saj se lahko pri
     * disassemblerju mimogrede zgodi, da gledamo podatke ob ukazih.
     * 
     * @return prefix
     */
    public String getDescriptionPrefix() {
        // dolocilo za extended nacin
        String prefix = extended ? "+" : "";
        // dolocilo za PC/B
        switch((xbpe & 0x06)>>1) {
            case 0: return prefix;
            case 1: return prefix + "(PC)+";
            case 2: return prefix + "(B)+";
            default: return prefix + "???";
        }
    }
    /**
     * Vrne postfix, ki ga je potrebno dodati mnemoniku ukaza.
     * V osnovi je postfix le v primeru uporabe indeksnega naslavljanja.
     * @return postfix
     */
    public String getDescriptionPostfix() {
        return (xRelative ? ", X" : "");
    }
}
