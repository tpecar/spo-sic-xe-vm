/* Tadej Pečar, SPO 2016/2017 */
package vm.machine;

import vm.event.Access;
import vm.event.EventDispatcher;
import vm.machine.device.FileDevice;

/**
 * Hrani strojne ukaze ter pripadajoco logiko.
 * @author tpecar
 */
public class InstructionSet {
    /**
     * Dispatch tabela za strojne ukaze.
     * Hkrati kot lookup tabela za imena ukazov, kar uporabimo pri
     * disassemblerju.
     */
    public static final Instruction INSTRUCTION[] = {
        // LDA 0x00 0x00
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                state.writeRegister24((int)operand.read(3), state.A.getIndex());
            }
            @Override public String getMnemonic() {return "LDA";}
        },
        // LDX 0x04 0x01
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                state.writeRegister24((int)operand.read(3), state.X.getIndex());
            }
            @Override public String getMnemonic() {return "LDX";}
        },
        // LDL 0x08 0x02
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                state.writeRegister24((int)operand.read(3), state.L.getIndex());
            }
            @Override public String getMnemonic() {return "LDL";}
        },
        // STA 0x0C 0x03
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                operand.write(state.readRegister24(state.A.getIndex()),3);
            }
            @Override public String getMnemonic() {return "STA";}
        },
        // STX 0x10 0x04
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                operand.write(state.readRegister24(state.X.getIndex()),3);
            }
            @Override public String getMnemonic() {return "STX";}
        },
        // STL 0x14 0x05
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                operand.write(state.readRegister24(state.L.getIndex()),3);
            }
            @Override public String getMnemonic() {return "STL";}
        },
        // ADD 0x18 0x06
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                state.writeRegister24(
                    (int)(state.readRegister24(state.A.getIndex()))+
                    (int)(operand.read(3)),
                    state.A.getIndex()
                );
            }
            @Override public String getMnemonic() {return "ADD";}
        },
        // SUB 0x1C 0x07
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                state.writeRegister24(
                    (int)(state.readRegister24(state.A.getIndex()))-
                    (int)(operand.read(3)),
                    state.A.getIndex()
                );
            }
            @Override public String getMnemonic() {return "SUB";}
        },
        // MUL 0x20 0x08
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                state.writeRegister24(
                    (int)(state.readRegister24(state.A.getIndex()))*
                    (int)(operand.read(3)),
                    state.A.getIndex()
                );
            }
            @Override public String getMnemonic() {return "MUL";}
        },
        // DIV 0x24 0x09
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                state.writeRegister24(
                    (int)(state.readRegister24(state.A.getIndex()))/
                    (int)(operand.read(3)),
                    state.A.getIndex()
                );
            }
            @Override public String getMnemonic() {return "DIV";}
        },
        // COMP 0x28 0x0A
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                state.setCC((int)(state.readRegister24(state.A.getIndex()))-
                            (int)(operand.read(3)));
            }
            @Override public String getMnemonic() {return "COMP";}
        },
        // TIX 0x2C 0x0B
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                int x = state.readRegister24(state.X.getIndex())+1;
                state.writeRegister24(x, state.X.getIndex());
                
                state.setCC(x-(int)(operand.read(3)));
            }
            @Override public String getMnemonic() {return "TIX";}
        },
        // JEQ 0x30 0x0C
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                if((state.readRegister24(state.SW.getIndex()) & 0xC0) == 0x40)
                    state.writeRegister24((int)operand.interpreter.getTA(), state.PC.getIndex());
            }
            @Override public String getMnemonic() {return "JEQ";}
        },
        // JGT 0x34 0x0D
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                if((state.readRegister24(state.SW.getIndex()) & 0xC0) == 0x80)
                    state.writeRegister24((int)operand.interpreter.getTA(), state.PC.getIndex());
            }
            @Override public String getMnemonic() {return "JGT";}
        },
        // JLT 0x38 0x0E
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                if((state.readRegister24(state.SW.getIndex()) & 0xC0) == 0x00)
                    state.writeRegister24((int)operand.interpreter.getTA(), state.PC.getIndex());
            }
            @Override public String getMnemonic() {return "JLT";}
        },
        // J 0x3C 0x0F
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                state.writeRegister24((int)operand.interpreter.getTA(), state.PC.getIndex());
            }
            @Override public String getMnemonic() {return "J";}
        },
        // AND 0x40 0x10
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                state.writeRegister24(
                    (int)(state.readRegister24(state.A.getIndex()))&
                    (int)(operand.read(3)),
                    state.A.getIndex()
                );
            }
            @Override public String getMnemonic() {return "AND";}
        },
        // OR 0x44 0x11
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                state.writeRegister24(
                    (int)(state.readRegister24(state.A.getIndex()))|
                    (int)(operand.read(3)),
                    state.A.getIndex()
                );
            }
            @Override public String getMnemonic() {return "OR";}
        },
        // JSUB 0x48 0x12
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                state.writeRegister24(state.readRegister24(state.PC.getIndex()),
                                      state.L.getIndex());
                state.writeRegister24((int)operand.interpreter.getTA(), state.PC.getIndex());
            }
            @Override public String getMnemonic() {return "JSUB";}
        },
        // RSUB 0x4C 0x13
        // ta ukaz bi sicer lahko na SIC/XE arhitekturi bil tipa 1, vendar je
        // obstajal ze na SIC arhitekturi, kjer je edini format 3S, s cimer se
        // operand zanemari - to se je ohranilo na SIC/XE
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                state.writeRegister24(state.readRegister24(state.L.getIndex()),
                                      state.PC.getIndex());
            }
            @Override public String getMnemonic() {return "RSUB";}
        },
        // LDCH 0x50 0x14
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                // zgornji biti registra A se ne ohranjajo (?)
                state.writeRegister24((int)operand.read(1), state.A.getIndex());
            }
            @Override public String getMnemonic() {return "LDCH";}
        },
        // STCH 0x54 0x15
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                operand.write(state.readRegister24(state.A.getIndex()), 1);
            }
            @Override public String getMnemonic() {return "STCH";}
        },
        // ADDF 0x58 0x16
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                double f = state.F.get48double();
                EventDispatcher.dispatchRegisterEvent(state.F, Access.READ);
                
                state.F.set48double(f + Double.longBitsToDouble(operand.read(6)<<16));
                EventDispatcher.dispatchRegisterEvent(state.F, Access.WRITE);
            }
            @Override public String getMnemonic() {return "ADDF";}
        },
        // SUBF 0x5C 0x17
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                double f = state.F.get48double();
                EventDispatcher.dispatchRegisterEvent(state.F, Access.READ);
                
                state.F.set48double(f - Double.longBitsToDouble(operand.read(6)<<16));
                EventDispatcher.dispatchRegisterEvent(state.F, Access.WRITE);
            }
            @Override public String getMnemonic() {return "SUBF";}
        },
        // MULF 0x60 0x18
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                double f = state.F.get48double();
                EventDispatcher.dispatchRegisterEvent(state.F, Access.READ);
                
                state.F.set48double(f * Double.longBitsToDouble(operand.read(6)<<16));
                EventDispatcher.dispatchRegisterEvent(state.F, Access.WRITE);
            }
            @Override public String getMnemonic() {return "MULF";}
        },
        // DIVF 0x64 0x19
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                double f = state.F.get48double();
                EventDispatcher.dispatchRegisterEvent(state.F, Access.READ);
                
                state.F.set48double(f / Double.longBitsToDouble(operand.read(6)<<16));
                EventDispatcher.dispatchRegisterEvent(state.F, Access.WRITE);
            }
            @Override public String getMnemonic() {return "DIVF";}
        },
        // LDB 0x68 0x1A
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                state.writeRegister24((int)operand.read(3), state.B.getIndex());
            }
            @Override public String getMnemonic() {return "LDB";}
        },
        // LDS 0x6C 0x1B
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                state.writeRegister24((int)operand.read(3), state.S.getIndex());
            }
            @Override public String getMnemonic() {return "LDS";}
        },
        // LDF 0x70 0x1C
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                state.F.set48(operand.read(6));
                EventDispatcher.dispatchRegisterEvent(state.F, Access.WRITE);
            }
            @Override public String getMnemonic() {return "LDF";}
        },
        // LDT 0x74 0x1D
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                state.writeRegister24((int)operand.read(3), state.T.getIndex());
            }
            @Override public String getMnemonic() {return "LDT";}
        },
        // STB 0x78 0x1E
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                operand.write(state.readRegister24(state.B.getIndex()), 3);
            }
            @Override public String getMnemonic() {return "STB";}
        },
        // STS 0x7C 0x1F
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                operand.write(state.readRegister24(state.S.getIndex()), 3);
            }
            @Override public String getMnemonic() {return "STS";}
        },
        // STF 0x80 0x20
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                long f = state.F.get48();
                EventDispatcher.dispatchRegisterEvent(state.F, Access.READ);
                operand.write(f, 6);
            }
            @Override public String getMnemonic() {return "STF";}
        },
        // STT 0x84 0x21
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                operand.write(state.readRegister24(state.T.getIndex()), 3);
            }
            @Override public String getMnemonic() {return "STT";}
        },
        // COMPF 0x88 0x22
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                double f = state.F.get48double();
                EventDispatcher.dispatchRegisterEvent(state.F, Access.READ);
                
                state.setCC(f - Double.longBitsToDouble(operand.read(6)<<16));
            }
            @Override public String getMnemonic() {return "COMPF";}
        },
        // ???? 0x8C 0x23
        null,
        // ADDR 0x90 0x24
        new Instruction2() {
            @Override public void execute(State state, Operand2 operand) {
                operand.writeRegister2(operand.readRegister2() +
                        operand.readRegister1());
            }
            @Override public String getMnemonic() {return "ADDR";}
        },
        // SUBR 0x94 0x25
        new Instruction2() {
            @Override public void execute(State state, Operand2 operand) {
                operand.writeRegister2(operand.readRegister2() -
                        operand.readRegister1());
            }
            @Override public String getMnemonic() {return "SUBR";}
        },
        // MULR 0x98 0x26
        new Instruction2() {
            @Override public void execute(State state, Operand2 operand) {
                operand.writeRegister2(operand.readRegister2() *
                        operand.readRegister1());
            }
            @Override public String getMnemonic() {return "MULR";}
        },
        // DIVR 0x9C 0x27
        new Instruction2() {
            @Override public void execute(State state, Operand2 operand) {
                operand.writeRegister2(operand.readRegister2() /
                        operand.readRegister1());
            }
            @Override public String getMnemonic() {return "DIVR";}
        },
        // COMPR 0xA0 0x28
        new Instruction2() {
            @Override public void execute(State state, Operand2 operand) {
                state.setCC((int)(operand.readRegister1())-
                            (int)(operand.readRegister2()));
            }
            @Override public String getMnemonic() {return "COMPR";}
        },
        // pri shift funkcijah moramo +1 k vrednosti operanda
        // torej operand 0 ti pomeni shift za 1
        // SHIFTL 0xA4 0x29
        new Instruction2() {
            @Override public void execute(State state, Operand2 operand) {
                int r1 = (int)operand.readRegister1();
                // imamo krozni shift v levo
                state.writeRegister24(r1 << (operand.r2idx+1) | r1 >>> (23-operand.r2idx), 0);
            }
            @Override public String getMnemonic() {return "SHIFTL";}
        },
        // SHIFTR 0xA8 0x2A
        new Instruction2() {
            @Override public void execute(State state, Operand2 operand) {
                // prvo izvedemo shift v levo, zato da Javin aritmeticni shift
                // uposteva nas 24. bit
                operand.writeRegister1((operand.readRegister1()<<8)>> (9+operand.r2idx));
            }
            @Override public String getMnemonic() {return "SHIFTR";}
        },
        // RMO 0xAC 0x2B
        new Instruction2() {
            @Override public void execute(State state, Operand2 operand) {
                operand.writeRegister2(operand.readRegister1());
            }
            @Override public String getMnemonic() {return "RMO";}
        },
        // SVC 0xB0 0x2C
        new Instruction2() {
            @Override public void execute(State state, Operand2 operand) {
                InstructionSet.notImplemented(state, getMnemonic());
            }
            @Override public String getMnemonic() {return "SVC";}
        },
        // CLEAR 0xB4 0x2D
        // 2. operand se ignorira
        new Instruction2() {
            @Override public void execute(State state, Operand2 operand) {
                operand.writeRegister1(0);
            }
            @Override public String getMnemonic() {return "CLEAR";}
        },
        // TIXR 0xB8 0x2E
        new Instruction2() {
            @Override public void execute(State state, Operand2 operand) {
                int x = state.readRegister24(state.X.getIndex())+1;
                state.writeRegister24(x, state.X.getIndex());
                
                state.setCC(x-operand.readRegister1());
            }
            @Override public String getMnemonic() {return "TIXR";}
        },
        // ???? 0xBC 0x2F
        null,
        // FLOAT 0xC0 0x30
        new Instruction1() {
            @Override public void execute(State state) {
                state.F.set48double(state.readRegister24(state.A.getIndex()));
                EventDispatcher.dispatchRegisterEvent(state.F, Access.WRITE);
            }
            @Override public String getMnemonic() {return "FLOAT";}
        },
        // FIX 0xC4 0x31
        new Instruction1() {
            @Override public void execute(State state) {
                double f = state.F.get48double();
                EventDispatcher.dispatchRegisterEvent(state.F, Access.READ);
                state.writeRegister24((int)f, state.A.getIndex());
            }
            @Override public String getMnemonic() {return "FIX";}
        },
        // NORM 0xC8 0x32
        new Instruction1() {
            @Override public void execute(State state) {
                InstructionSet.notImplemented(state, getMnemonic());
            }
            @Override public String getMnemonic() {return "NORM";}
        },
        // ???? 0xCC 0x33
        null,
        // LPS 0xD0 0x34
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                InstructionSet.notImplemented(state, getMnemonic());
            }
            @Override public String getMnemonic() {return "LPS";}
        },
        // STI 0xD4 0x35
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                InstructionSet.notImplemented(state, getMnemonic());
            }
            @Override public String getMnemonic() {return "STI";}
        },
        // RD 0xD8 0x36
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                int devIdx = (int)operand.read(1);
                
                if(state.device[devIdx]==null)
                    // ce naprava se ne obstaja, pred branjem odpremo FileDevice zanjo
                    state.device[devIdx] = new FileDevice(
                            String.format("F%02d.dev", devIdx));
                            
                // upostevati moramo, da beremo nepredznaceno
                // (Java bo zgornji bit samodejno razsirila)
                state.writeRegister24(((int)state.device[devIdx].read()) & 0xFF,
                                      state.A.getIndex());
            }
            @Override public String getMnemonic() {return "RD";}
        },
        // WD 0xDC 0x37
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                int devIdx = (int)operand.read(1);
                
                if(state.device[devIdx]==null)
                    state.device[devIdx] = new FileDevice(
                            String.format("F%02d.dev", devIdx));
                
                state.device[devIdx].write(
                        (byte)state.readRegister24(state.A.getIndex())
                );
            }
            @Override public String getMnemonic() {return "WD";}
        },
        // TD 0xE0 0x38
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                int devIdx = (int)operand.read(1);
                int isReady;
                if(state.device[devIdx]==null)
                    // ce naprava se ne obstaja, ustvarimo novo
                    state.device[devIdx] = new FileDevice(
                            String.format("F%02d.dev", devIdx));
                isReady = state.device[devIdx].test() ? 1 : 0;
                state.setCC(isReady);
            }
            @Override public String getMnemonic() {return "TD";}
        },
        // ???? 0xE4 0x39
        null,
        // STSW 0xE8 0x3A
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                operand.write(state.readRegister24(state.SW.getIndex()), 3);
            }
            @Override public String getMnemonic() {return "STSW";}
        },
        // SSK 0xEC 0x3B
        new Instruction34() {
            @Override public void execute(State state, Operand34 operand) {
                InstructionSet.notImplemented(state, getMnemonic());
            }
            @Override public String getMnemonic() {return "SSK";}
        },
        // SIO 0xF0 0x3C
        new Instruction1() {
            @Override public void execute(State state) {
                InstructionSet.notImplemented(state, getMnemonic());
            }
            @Override public String getMnemonic() {return "SIO";}
        },
        // HIO 0xF4 0x3D
        new Instruction1() {
            @Override public void execute(State state) {
                InstructionSet.notImplemented(state, getMnemonic());
            }
            @Override public String getMnemonic() {return "HIO";}
        },
        // TIO 0xF8 0x3E
        new Instruction1() {
            @Override public void execute(State state) {
                InstructionSet.notImplemented(state, getMnemonic());
            }
            @Override public String getMnemonic() {return "TIO";}
        },
    };
    // 63 operacijskih kod, od tega 4 ne pripadajo nobenemu ukazu
    // s tem je skupno 59 dejanskih ukazov.
    
    private static void notImplemented(State state, String mnemonic) {
        throw new UnsupportedOperationException(
            String.format("[inst addr: %05X] %s is not implemented",
                          state.oldPC.get24(), mnemonic)
        );
    }
}
