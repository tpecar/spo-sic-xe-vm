/* Tadej Pečar, SPO 2016/2017 */
package vm.machine;

/**
 * Tip ukaza.
 * @author tpecar
 */
public enum InstructionType {
    TYPE1,
    TYPE2,
    TYPE34
}
