/* Tadej Pečar, SPO 2016/2017 */
package vm.machine.device;

import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Privzeta I/O naprava.
 * Bere/pise v datoteko - tipicno je datoteka poimenovana enako kot naslov
 * naprave.
 * @author tpecar
 */
public class FileDevice implements Device {
    /**
     * Status naprave.
     * Ob prvi izjemi ta postane false (zal arhitektura kottaka ne omogoca, da
     * bi napravo resetirali ipd. tako da je ta od prve izjeme naprej
     * neuporabna)
     */
    boolean ready;
    RandomAccessFile file;
    
    String fileName; /* za napake */
    
    /* konstruktor */
    public FileDevice(String fileName) {
        this.fileName = fileName;
        try {
            file = new RandomAccessFile(fileName, "rw");
            file.seek(0);
        }
        catch (IOException e) {
            System.err.format("Failed to instantiate file device [%s]: %s\n",
                    fileName,
                    e.toString());
            ready = false;
        }
        ready = true;
    }
    
    @Override
    public boolean test() {
        return ready;
    }
    @Override
    public byte read() {
        if(ready) {
            try {
                return (byte)file.read();
            }
            catch(IOException e) {
                System.err.format("File device [%s] read failed: %s\n",
                        fileName,
                        e.toString());
                ready = false;
            }
            return 0;
        }
        else
            return 0;
    }
    @Override
    public void write(byte value) {
        if(ready) {
            try {
                file.write(value);
            }
            catch(IOException e) {
                System.err.format("File device [%s] write failed: %s\n",
                        fileName,
                        e.toString());
                ready = false;
            }
        }
    }
}
