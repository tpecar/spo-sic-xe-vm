/* Tadej Pečar, SPO 2016/2017 */
package vm.machine.device;

/**
 * Naprava, do katere dostopamo preko TD, RD, WD ukazov.
 * So dostopne znotraj svojega 8-bitnega naslovnega prostora naprav.
 * 
 * Vse izjeme se potihoma ignorirajo - v najboljsem primeru lahko nastavimo
 * zastavico, ki naslednji test naredi false.
 * 
 * @author tpecar
 */
public interface Device {
    /**
     * Ce je naprava pripravljena za delo (tj mozno branje/pisanje, ne nujno
     * oboje).
     * @return status naprave
     */
    boolean test();
    /**
     * Prebere 1 bajt.
     * @return prebran byte
     */
    byte read();
    /**
     * Zapise 1 bajt.
     * @param value zapisan byte
     */
    void write(byte value);
}
