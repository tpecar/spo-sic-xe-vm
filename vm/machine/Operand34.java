/* Tadej Pečar, SPO 2016/2017 */
package vm.machine;

import vm.event.MemoryAccess;

/**
 * Skrbi za pravilno interpretacijo, branje ter zapis operanda pri ukazu formata
 * S3/X3/4.
 * Tezavno je namrec, da pri posameznem ukazu ne vemo, kaj bo ta pocel (ali bo
 * bral ali pisal - posledicno odpade da bi podali kar dereferencirano vrednost
 * operanda), prav tako ne vemo kaksen je sam operand (mi namrec ne moremo
 * v samem ukazu predpostaviti, da vedno beremo iz pomnilnika, lahko imamo tudi
 * immediate naslavljanje).
 * 
 * Za objekte danega razreda ni predvideno, da bi imel dolgo zivljensko dobo
 * (obstajajo le za izvedbo posameznega ukaza).
 * 
 * @author tpecar
 */
public class Operand34 {
    /** Stanje pomnilnika, iz katerega crpamo ukaz. */
    private final State state;
    /** Naslov ukaza, katerega operand interpretiramo. */
    private final int address;
    public final Operand34Interpreter interpreter;

    public Operand34(State state, int address) {
        this.state = state;
        this.address = address;
        
        interpreter = new Operand34Interpreter(
                state.mem, address,
                state.PC.get24(),
                state.B.get24(),
                state.X.get24()
        );
    }
    /* za tvorbo izjeme v primeru, ce zelimo narediti dostop z neveljavnim TA */
    private void checkTA() {
        if (!interpreter.isValid())
            throw new UnsupportedOperationException(
                String.format("[instruction addr: %05X] Invalid Target Address [xbpe: %02X]",
                              address, interpreter.getXBPE())
            );
    }

    /**
     * Prebere operand.
     * Tu upostevamo, da gre za big endian arhitekturo.
     * 
     * @param numBytes stevilo Byte-ov, ki jih preberemo za vrednost
     * @return vrednost operanda
     */
    public long read(int numBytes) {
        checkTA();
        switch(interpreter.getNI() & 0x03) {
            // SIC/simple addressing
            case 0: case 3:
                return state.readMemValue(interpreter.getTA(),
                        numBytes, MemoryAccess.SIMPLE_OPERAND);
            // immediate addressing
            case 1:
                return interpreter.getTA();
            // indirect addressing
            case 2:
                return state.readMemValue(
                    (int)state.readMemValue(interpreter.getTA(),
                            3, MemoryAccess.INDIRECT_OPERAND_PTR),
                    numBytes, MemoryAccess.INDIRECT_OPERAND_VALUE
                );
            default:
                throw new IllegalStateException();
        }
    }
    /**
     * Zapise operand.
     * Pri tem je pisanje v primeru immediate naslavljanja nedovoljeno.
     * Upostevamo big endian.
     * 
     * @param value vrednost, ki jo zapisujemo v pomnilnik
     * @param numBytes stevilo Byte-ov, na katere zapisemo vrednost
     */
    public void write(long value, int numBytes) {
        checkTA();
        switch(interpreter.getNI() & 0x03) {
            // SIC/simple addressing
            case 0: case 3:
                state.writeMemValue(value, interpreter.getTA(),
                        numBytes, MemoryAccess.SIMPLE_OPERAND);
                break;
            // immediate addressing
            case 1:
                throw new UnsupportedOperationException(
                    String.format("[instruction addr: %05X] Cannot write to immediate",
                                  address)
                );
            // indirect addressing
            case 2:
                state.writeMemValue(
                    value,
                    (int)state.readMemValue(interpreter.getTA(),
                            3, MemoryAccess.INDIRECT_OPERAND_PTR),
                    numBytes, MemoryAccess.INDIRECT_OPERAND_VALUE);
                break;
            default:
                throw new IllegalStateException();
        }
    }
}
