/* Tadej Pečar, SPO 2016/2017 */
package vm.machine;

/**
 *
 * @author tpecar
 */
public class Operand2 {
    /** Stanje pomnilnika, iz katerega crpamo vrednosti registrov. */
    private final State state;
    /** Naslov ukaza, katerega operand interpretiramo. */
    private final int address;
    
    /* dejanska registra */
    public final int r1idx;
    public final int r2idx;
    private final boolean regValid;
    
    public Operand2(State state, int address) {
        this.state = state;
        this.address = address;
        
        r1idx = (state.mem[address+1] & 0xF0)>>4;
        r2idx = (state.mem[address+1] & 0x0F);

        regValid = r1idx < state.register.length && state.register[r1idx]!=null &&
                   r2idx < state.register.length && state.register[r2idx]!=null;
    }
    /**
     * Vrne ali je operand sploh veljaven.
     * @return ali sta oba registra veljavna
     */
    public boolean isValid() {
        return regValid;
    }
    /* za tvorbo izjeme v primeru dostopa z neveljavnim operandom */
    private void checkReg() {
        if (!regValid)
            throw new UnsupportedOperationException(
                String.format("[instruction addr: %05X] Invalid register index [r1r2: %02X]",
                              address, state.mem[address+1] & 0xFF)
            );
    }
    /**
     * Vrne vrednost 1 operandnega registra.
     * @return vrednost registra
     */
    public int readRegister1() {
        checkReg();
        return state.readRegister24(r1idx);
    }
    /**
     * Zapise vrednost 1 operandnega registra.
     * @param value nova vrednost
     */
    public void writeRegister1(int value) {
        checkReg();
        state.writeRegister24(value, r1idx);
    }
    /**
     * Vrne vrednost 2 operandnega registra.
     * @return vrednost registra
     */
    public int readRegister2() {
        checkReg();
        return state.readRegister24(r2idx);
    }
    /**
     * Zapise vrednost 2 operandnega registra. 
     * @param value nova vrednost
     */
    public void writeRegister2(int value) {
        checkReg();
        state.writeRegister24(value, r2idx);
    }
    /**
     * Vrne opis 1. registra v obliki niza.
     * Za namen disassemblerja.
     * @return niz, ki opisuje, katera registra se uporabita kot operanda
     */
    public String describeRegister1() {
        return (regValid ? state.register[r1idx].toString() : "??");
    }
    /**
     * Vrne opis 2. registra v obliki niza.
     * Za namen disassemblerja.
     * @return niz, ki opisuje, katera registra se uporabita kot operanda
     */
    public String describeRegister2() {
        return (regValid ? state.register[r2idx].toString() : "??");
    }
}
