/* Tadej Pečar, SPO 2016/2017 */
package vm.machine;

/**
 * Operacija ukaza tipa 2.
 * @author tpecar
 */
public abstract class Instruction2 implements Instruction {
    /**
     * Operacija, ki ga ukaz izvede.
     * 
     * @param state stanje VM, ki ga tekom izvajanja ukaza spreminjamo
     * @param operand registri, nad katerimi ukaz izvaja operacijo
     */
    public abstract void execute(State state, Operand2 operand);
    
    @Override
    public InstructionType getType() {
        return InstructionType.TYPE2;
    }
}
