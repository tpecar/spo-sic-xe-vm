/* Tadej Pečar, SPO 2016/2017 */
package vm.machine.register;

/**
 * Za genericen prenos podatkov med registri.
 * Potrebno zaradi ukazov formata 2.
 * 
 * Prav tako je zazeleno, da 
 * 
 * @author tpecar
 */
public abstract class Register {
    private final String name;
    private final int index;
    
    public Register(String name, int index) {
        this.name = name;
        this.index = index;
    }
    
    /**
     * Ponastavi vsebino registra na 0.
     */
    public abstract void clear();
    /**
     * Vrne vsebino registra.
     * @return spodnjih 24 bitov registra
     */
    public abstract int get24();
    /**
     * Nastavi spodnjih 24 bitov registra.
     * @param value vrednost, ki se nastavi spodnjim 24 bitom
     *              privzemimo, da ce je register vecji, da se zgornji biti
     *              ohranijo
     */
    public abstract void set24(int value);
    
    /**
     * Vrne indeks registra.
     * @return indeks registra
     */
    public int getIndex() {
        return this.index;
    }
    /**
     * Vrne ime registra.
     * @return ime registra
     */
    @Override
    public String toString()
    {
        return name;
    }
}