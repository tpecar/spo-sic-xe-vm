/* Tadej Pečar, SPO 2016/2017 */
package vm.machine.register;

/**
 * Za 24-bitni celostevilski register.
 * @author tpecar
 */
public class RegisterInt extends Register {
    int value;
    
    public RegisterInt(String name, int index) {
        super(name, index);
    }
    
    @Override
    public void clear() {
        value = 0;
    }
    @Override
    public int get24() {
        return value & 0x00FFFFFF;
    }
    @Override
    public void set24(int value) {
        this.value = value & 0x00FFFFFF;
    }
}
