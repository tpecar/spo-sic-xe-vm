/* Tadej Pečar, SPO 2016/2017 */
package vm.machine.register;

/**
 * 48bit float register.
 * Ker je float register omejen na 48 bitov, vzamemo le zgornjih 48 bitov
 * double vrednosti (s ter e identicna, le m odrezemo).
 * 
 * Uposteval reprezentacijo na
 * http://www.javaworld.com/article/2077257/learn-java/floating-point-arithmetic.html
 * 
 * @author tpecar
 */
public class RegisterFloat extends Register {
    double value;

    public RegisterFloat(String name, int index) {
        super(name, index);
    }
    
    @Override
    public void clear() {
        value = 0.0;
    }
    @Override
    public int get24() {
        /* odrezemo spodnjih 16 bitov mantise, nato od dobljene 48bitne
           vrednosti vzamemo le spodnjih 24 bitov */
        return (int)((Double.doubleToRawLongBits(value)>>>16) &
                0x0000000000FFFFFFl);
    }
    @Override
    public void set24(int value) {
        this.value = Double.longBitsToDouble(((long)value &
                0x0000000000FFFFFFl)<<16);
    }
    /**
     * Vrne 48bitno reprezentacijo float registra.
     * Za izpis "celotne" (tj. celotne z vidika natancnosti SIC/XE)
     * vsebine registra ipd.
     * @return celotna vrednost float registra v celostevilski obliki
     */
    public long get48() {
        return (Double.doubleToRawLongBits(value)>>>16) &
                0x0000FFFFFFFFFFFFl;
    }
    /**
     * Zapise 48bitno reprezentacijo float registra.
     * @param value celotna vrednost float registra
     */
    public void set48(long value) {
        // zgornjih 16 bitov izgubimo ze zaradi shifta, spodnjih 16 pa 0, kar je
        // zazeljeno (mi imamo namrec za 16 bit preveliko mantiso)
        this.value = Double.longBitsToDouble(value<<16);
    }
    /**
     * Vrne dejansko vrednost float registra.
     * @return nespremenjena vrednost v plavajoci vejici
     */
    public double get48double() {
        return value;
    }
    /**
     * Nastavi dejansko vrednost float registra.
     * Zaradi omejitev SIC se natancnost zmanjsa.
     * @param value nova vrednost
     */
    public void set48double(double value) {
        this.value = Double.longBitsToDouble(
                Double.doubleToRawLongBits(value) & 0xFFFFFFFFFFFF0000l
        );
    }
}
