/* Tadej Pečar, SPO 2016/2017 */
package vm.machine;

/**
 * Operacija ukaza tipa 1.
 * Preko tega naredimo dispatch tabelo.
 * 
 * @author tpecar
 */
public abstract class Instruction1 implements Instruction {
    /**
     * Operacija, ki ga ukaz izvede.
     * Ta vpliva na na podano stanje, hkrati pa prozi dogodke preko
     * globalnega EventDispatcher.
     * 
     * @param state stanje VM, ki ga tekom izvajanja ukaza spreminjamo
     */
    public abstract void execute(State state);
    
    @Override
    public InstructionType getType(){
        return InstructionType.TYPE1;
    }
}
