/* Tadej Pečar, SPO 2016/2017 */
package vm.machine;

/**
 * Operacija ukaza tipa 3/4.
 * @author tpecar
 */
public abstract class Instruction34 implements Instruction {
    /**
     * Operacija, ki ga ukaz izvede.
     * 
     * @param state stanje VM, ki ga tekom izvajanja ukaza spreminjamo
     * @param operand operand, dejanska vrednost (to je bodisi vrednost, ki je
     *                bila prebrana iz uporabnega naslova, oz. sam uporabni
     *                naslov v primeru immediate nacina dostopa).
     * 
     * Prvotno je to 12 (format 3), 15 (SIC format) ali 20 (format 4) bitno celo
     * stevilo, mi pravilno interpretacijo ukaza prepustimo klicatelju, pri
     * cemer nam ta Operand objekt, ki abstrahira vse operacije s pomnilnikom.
     */
    public abstract void execute(State state, Operand34 operand);
    
    @Override
    public InstructionType getType() {
        return InstructionType.TYPE34;
    }
}
