/* Tadej Pečar, SPO 2016/2017 */
package vm.machine;

import vm.event.Access;
import vm.event.EventDispatcher;
import vm.event.MemoryAccess;
import vm.machine.device.Device;
import vm.machine.register.RegisterInt;
import vm.machine.register.RegisterFloat;
import java.io.IOException;
import static vm.machine.InstructionSet.INSTRUCTION;
import vm.machine.register.Register;

/**
 * Hrani trenutno stanje virtualnega stroja.
 * @author tpecar
 */
public class State {

    /* registri */
    public final RegisterInt A, /* akumulator */
                             X, /* indeksni register */
                             L, /* povezovalni register */
                             B, /* bazni rekgister */
                             S, /* splosnonamenski register */
                             T, /* splosnonamenski register */
                             PC, /* programski stevec */
                             SW; /* statusni register */
    public final RegisterFloat F; /* register za operacije s plavajoco vejico */
    /* 
    Ker se pri fetch ciklu ze premaknemo na naslednji ukaz, si moramo posebej
    hraniti programski stevec trenutno izvajanega ukaza - to se uporabi
    predvsem pri izpisu (v primeru izjem ter eventov, pri cemer v interpreter
    operanda podamo PC pred povecanjem, da pravilno dolocimo UN za izpis).
    
    Prav tako uporabljamo za ugotovitev, ce smo prisli do halt and catch fire
    zanke.
    */
    public RegisterInt oldPC;
    
    /* tabela registrov */
    public final Register[] register;
    
    /* pomnilnik */
    public final byte[] mem;
    
    /* I/O naprave */
    public final Device[] device;
    
    /* konstruktor */
    public State() {
        /* inicializiramo registre ter indeksno tabelo */
        register = new Register[10];
        
        register[0] = A = new RegisterInt("A",0);
        register[1] = X = new RegisterInt("X",1);
        register[2] = L = new RegisterInt("L",2);
        register[3] = B = new RegisterInt("B",3);
        register[4] = S = new RegisterInt("S",4);
        register[5] = T = new RegisterInt("T",5);
        register[6] = PC = new RegisterInt("PC",6);
        oldPC = new RegisterInt("PC",6);
        register[7] = null; /* register z indeksom 7 ne obstaja */
        register[8] = SW = new RegisterInt("SW",8);
        register[9] = F = new RegisterFloat("F",9);
        
        /* inicializiramo 20bitni pomnilniski prostor */
        mem = new byte[1<<20];
        
        /* inicializiramo 8bitni prostor naprav */
        device = new Device[1<<8];
        
        /* inicializiramo stdin/stdout/stderr - razen ce tu inicializiramo se
           kaksne druge naprave, se za vse neinicializirane predpostavi, da so
           FileDevice - te se zgenerirajo on-the-fly, ko prvic dostopamo do
           njihovega naslova
        */
        device[0] = new Device() {
            boolean ready=true;
            @Override public boolean test() {return ready;}
            /* ce stdin ne dela sploh ne poskusamo javiti napake, ker bi
               to vnovic povzrocilo napako */
            @Override public byte read() {
                try {return (byte)System.in.read();}
                catch (IOException e) {ready = false;}
                return 0;
            }
            /* stdin je sink za pisanje */
            @Override public void write(byte value) {}
        };
        /* stdout, stderr nikoli ne vrneta izjeme, zato sta vedno ready */
        device[1] = new Device() {
            @Override public boolean test() {return true;}
            @Override public byte read(){return 0;}
            @Override public void write(byte value){System.out.write(value);}
        };
        device[2] = new Device() {
            @Override public boolean test() {return true;}
            @Override public byte read(){return 0;}
            @Override public void write(byte value){System.err.write(value);}
        };
    }
    /* funkcije za eksplicitno branje/pisanje v stanje - te prozijo evente za
       dostop */
    /**
     * Prebere multibyte vrednost iz pomnilnika.
     * Pri tem uposteva big-endian nacin.
     * 
     * @param address zacetni naslov vrednosti
     * @param numBytes stevilo bajtov, ki so vsebovane v vrednosti - do 8
     * @param ma razlog dostopa, ki se posreduje naprej v dogodku
     * @return vrednost
     */
    public long readMemValue(int address, int numBytes, MemoryAccess ma) {
        long value=0;
        for(int tByte=0; tByte<numBytes; tByte++) {
            value = value<<8;
            value += ((int)mem[address+tByte]) & 0xFF;
        }
        EventDispatcher.dispatchMemoryEvent(mem, address, address+numBytes,
                ma, Access.READ);
        return value;
    }
    /**
     * Zapise multibyte vrednost iz pomnilnika.
     * Pri tem uposteva big-endian nacin.
     * 
     * @param value vrednost, ki jo zelimo zapisati
     * @param address zacteni naslov zapisa vrednosti
     * @param numBytes stevilo bajtov vrednosti, ki se zapise - prvo so zapisani
     *                 nizji bajti od visjih proti nizjim pomnilniskim naslovom
     * @param ma razlog dostopa
     */
    public void writeMemValue(long value, int address, int numBytes, MemoryAccess ma) {
        for(int tByte=numBytes-1; tByte>=0; tByte--) {
            mem[address+tByte] = (byte)(value);
            value = value>>>8;
        }
        EventDispatcher.dispatchMemoryEvent(mem, address, address+numBytes,
                ma, Access.WRITE);
    }
    /**
     * Prebere vrednost iz registra.
     * 
     * @param idx indeks registra
     * @return vrednost registra
     */
    public int readRegister24(int idx) {
        int value = register[idx].get24();
        EventDispatcher.dispatchRegisterEvent(register[idx], Access.READ);
        return value;
    }
    /**
     * Zapise vrednost v register.
     * 
     * @param value vrednost, ki jo zelimo zapisati
     * @param idx indeks registra
     */
    public void writeRegister24(int value, int idx) {
        register[idx].set24(value);
        EventDispatcher.dispatchRegisterEvent(register[idx], Access.WRITE);
    }
    /**
     * Pridobi instrukcijo za podan pomnilniski naslov.
     * 
     * @param address naslov
     * @return instrukcija
     */
    public Instruction getInstruction(int address) {
        int opcode = (mem[address]>>2) & 0x3F;
        Instruction inst = null;
        if(opcode < INSTRUCTION.length)
            inst=INSTRUCTION[opcode];
        return inst;
    }
    // za nastavitev CC znotraj SW
    /**
     * Nastavi CC (Condition Code) bite znotraj SW registra.
     * @param comp rezultat razlike med primerjanima vrednostima
     */
    public void setCC(double comp) {
        writeRegister24(
            (SW.get24() & (~0xC0)) | // brez eventa ker le "vpisujemo"
                /* SW[7:6] <-              00b    01b     10b */
                (comp <= 0 ? (comp < 0 ? 0x00 : 0x40) : 0x80 ),
            SW.getIndex()
       );
    }
}
